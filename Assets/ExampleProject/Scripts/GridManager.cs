﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class GridManager : MonoBehaviour
{
    public List<Sprite> Sprites = new List<Sprite>();
    public GameObject TilePrefab;
    [Space]
    public Sprite blockSprite;
    public Vector2Int[] obstacles;
    [Space]
    public int GridDimensionColumn = 8;
    public int GridDimensionRow = 8;
    public float Distance = 1.0f;
    private GameObject[,] Grid;

    public List<SpriteRenderer> SelectedBoxes = new List<SpriteRenderer>();


    public GameObject GameOverMenu;

    public static GridManager Instance { get; private set; }

    void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        Grid = new GameObject[GridDimensionColumn, GridDimensionRow];

        GameOverMenu.SetActive(false);
        InitGrid();

    }

    void InitGrid()
    {
        Vector3 positionOffset = transform.position - new Vector3(GridDimensionColumn * Distance / 2.0f, GridDimensionRow * Distance / 2.0f, 0);

        for (int row = 0; row < GridDimensionRow; row++)
        {
            for (int column = 0; column < GridDimensionColumn; column++)
            {
                GameObject newTile = Instantiate(TilePrefab);
                newTile.transform.localScale = new Vector3(.4f, .4f, .4f);
                newTile.layer = 8;

                List<Sprite> possibleSprites = new List<Sprite>(Sprites);

                //Choose what sprite to use for this cell
                Sprite left1 = GetSpriteAt(column - 1, row);
                Sprite left2 = GetSpriteAt(column - 2, row);
                if (left2 != null && left1 == left2)
                {
                    possibleSprites.Remove(left1);
                }

                Sprite down1 = GetSpriteAt(column, row - 1);
                Sprite down2 = GetSpriteAt(column, row - 2);
                if (down2 != null && down1 == down2)
                {
                    possibleSprites.Remove(down1);
                }

                SpriteRenderer renderer = newTile.GetComponentInChildren<SpriteRenderer>();
                renderer.sprite = possibleSprites[Random.Range(0, possibleSprites.Count)];

                Tile tile = newTile.transform.GetChild(0).gameObject.AddComponent<Tile>();
                tile.Position = new Vector2Int(column, row);

                newTile.transform.parent = transform;
                newTile.transform.position = new Vector3(column * Distance, row * Distance, 0) + positionOffset;

                Grid[column, row] = newTile.transform.GetChild(0).gameObject;
            }
        }


        for (int i = 0; i < obstacles.Length; i++)
        {
            Grid[obstacles[i].x, obstacles[i].y].GetComponent<SpriteRenderer>().sprite = blockSprite;
            Grid[obstacles[i].x, obstacles[i].y].GetComponent<Tile>().isObstacle = true;
            Grid[obstacles[i].x, obstacles[i].y].transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);

        }

    }

    Sprite GetSpriteAt(int column, int row)
    {
        if (column < 0 || column >= GridDimensionColumn
         || row < 0 || row >= GridDimensionRow)
            return null;
        GameObject tile = Grid[column, row];
        SpriteRenderer renderer = tile.GetComponent<SpriteRenderer>();
        return renderer.sprite;
    }

    SpriteRenderer GetSpriteRendererAt(int column, int row)
    {
        if (column < 0 || column >= GridDimensionColumn
         || row < 0 || row >= GridDimensionRow)
            return null;
        GameObject tile = Grid[column, row];
        SpriteRenderer renderer = tile.GetComponent<SpriteRenderer>();
        return renderer;
    }

    public bool CheckSelectedBoxed()
    {

        bool callBackAnswer = true;

        Sprite firstSprite = SelectedBoxes[0].sprite;

        for (int i = 1; i < SelectedBoxes.Count; i++)
        {
            if (SelectedBoxes[i].sprite != firstSprite)
                callBackAnswer = false;
        }

        if (callBackAnswer && SelectedBoxes.Count > 1)
        {
            //FindObjectOfType<Player>().speed += 0.5f;
            FindObjectOfType<Player>().SpeedUp(SelectedBoxes.Count);
            for (int j = 0; j < SelectedBoxes.Count; j++)
            {
                SelectedBoxes[j].GetComponent<Tile>().isEmpty = true;
                SelectedBoxes[j].transform.parent.DOScale(Vector3.zero, 0.2f).SetEase(Ease.InBack);


            }


            StartCoroutine(WaitForIt());

        }
        else
        {
            
            callBackAnswer = false;
            SelectedBoxes.Clear();
        }



        return callBackAnswer;

    }

    IEnumerator WaitForIt()
    {
        yield return new WaitForSeconds(.2f);

        for (int j = 0; j < SelectedBoxes.Count; j++)
        {
            SelectedBoxes[j].sprite = null;

        }

        if (SelectedBoxes.Count > 0)
        {
            PointText.instance.PointTrigger(SelectedBoxes.Count);
        }

        SelectedBoxes.Clear();


        FillHoles();

    }

    void FillHoles()
    {
        for (int column = 0; column < GridDimensionColumn; column++)
        {
            for (int row = 0; row < GridDimensionRow; row++)
            {
                
                while (GetSpriteRendererAt(column, row).sprite == null && !Grid[column, row].GetComponent<Tile>().isObstacle)
                {
                    SpriteRenderer current = GetSpriteRendererAt(column, row);
                    SpriteRenderer next = current;
                    for (int filler = row; filler < GridDimensionRow - 1; filler++)
                    {
                        next = GetSpriteRendererAt(column, filler + 1);
                        next.transform.parent.localScale = Vector3.zero;
                        current.sprite = next.sprite;
                        current = next;
                    }

                    next.sprite = Sprites[Random.Range(0, Sprites.Count)];
                }

            }
        }


        for (int column = 0; column < GridDimensionColumn; column++)
        {
            for (int row = 0; row < GridDimensionRow; row++)
            {
                Grid[column, row].transform.parent.DOScale(new Vector3(.4f, .4f, .4f), .2f).SetEase(Ease.OutBack);


            }
        }

    }


}
