using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameCoin : MonoBehaviour
{
    [SerializeField] Text moneyText;

    private void Update()
    {
        moneyText.text = PlayerPrefs.GetInt("Money").ToString();
    }
}
