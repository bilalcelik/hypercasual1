using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public GameObject internalPoint, end, interpolObstacle, endObstacle, interpolWall, endWall;

    bool move = true, climb = false;
    public float maxSpeed = 12, speed = 1, speedDuration = 3, speedMultiplyr = .75f;

    [Space]
    public float currentSpeedTime, currentSpeed;
    bool isSpeeding = false;

    Animator animator;

    public ParticleSystem particle, windParticle, flareParticle1, landing;
    public Canvas canvas;
    public Camera camera;
    Slider slider;

    public CanvasGroup myCG;
    bool flash = false, fireSpeed = false, endRun = false;

    public static Player instance;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

        currentSpeedTime = speedDuration;
        currentSpeed = speed;

        slider = GetComponentInChildren<Slider>();

        move = false;

        GameManager.instance.StartGame += StartTheGame;
        animator.SetBool("idle", true);
        animator.SetBool("run", false);


        windParticle.gameObject.SetActive(false);
        //flareParticle1.Stop();
        flareParticle1.Stop();
        landing.Stop();

        //flareParticle2.Stop();
    }

    public void StartTheGame()
    {
        RandomOpponentSelection.instance.StartGame();
    }

    public void StartGameWait()
    {
        move = true;
        //windParticle.gameObject.SetActive(true);

        animator.SetBool("idle", false);

        animator.SetBool("run", true);
    }

    // Update is called once per frame
    void Update()
    {
        if (flash)
        {
            myCG.alpha = myCG.alpha - (Time.deltaTime * 4);
            if (myCG.alpha <= 0)
            {
                myCG.alpha = 0;
                flash = false;
            }
        }

        canvas.transform.LookAt(camera.transform);
        // Move the object forward along its z axis 1 unit/second.
        if (move)
            transform.Translate(Vector3.forward * Time.deltaTime * currentSpeed);

        if (climb)
        {
            transform.Translate(Vector3.up * Time.deltaTime * currentSpeed);

        }

        if (isSpeeding)
        {
            windParticle.gameObject.SetActive(true);

            if (climb)
            {
                fireSpeed = false;
                flareParticle1.Stop();

            }

            if (currentSpeedTime > 0)
            {
                currentSpeedTime -= Time.deltaTime;
                currentSpeed -= (Time.deltaTime / 2);

            }
            else
            {
                isSpeeding = false;
                currentSpeedTime = speedDuration;

                if (currentSpeed > speed)
                    currentSpeed -= (Time.deltaTime);

                //currentSpeed = speed;
            }
        }
        else
        {
            flareParticle1.Stop();

            windParticle.gameObject.SetActive(false);

            if (currentSpeed > speed)
                currentSpeed -= (Time.deltaTime);
        }

        //slider.value = (currentSpeed - speed) / (maxSpeed - speed);

        if (slider.value > 0 && !isSpeeding)
            slider.value -= (Time.deltaTime / 2);

       


        if (endRun)
        {
            if (slider.value > 0)
            {
                //currentSpeed -= (Time.deltaTime / 5);
                currentSpeed = maxSpeed * 3;
                move = true;
            }
            else
            {
                EndGameRunEnd();
            }
        }
        else
        {
            currentSpeed = ((maxSpeed - speed) * slider.value) + speed;
        }


    }

    void EndGameRunEnd()
    {
       
        if (GameManager.instance.isWon)
            animator.SetBool("won", true);

        animator.SetBool("jump", false);
        animator.SetBool("run", false);

        move = false;
        endRun = false;


        GameManager.instance.EndGame();
    }

    public void SpeedUp(float count)
    {
        if (!GameManager.instance.endLock)
        {
            isSpeeding = true;

            currentSpeedTime = speedDuration;
            currentSpeed = currentSpeed + (count * speedMultiplyr);


            float value = slider.value;
            value += (count / 10);

            if (value > 1)
                value = 1;

            //DOTween.To(floatVal, floatVal, 1f, 1f);
            DOTween.To(() => slider.value, x => slider.value = x, value, .5f);


            if (count >= 4)
            {
                flareParticle1.Play();
                fireSpeed = true;

                myCG.alpha = 1;
                flash = true;
                camera.DOShakePosition(.5f, .3f, 10, 90);

                camera.DOKill();

                camera.DOFieldOfView((70), .5f).OnComplete(() =>
                {

                    camera.DOFieldOfView((60), 3f);
                });

            }



            if (currentSpeed > maxSpeed)
                currentSpeed = maxSpeed;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Jump")
        {
            animator.SetBool("jump", true);
            animator.SetBool("run", false);

            move = false;
            climb = false;
            //internalPoint = other.GetComponent<Jump>().interpolPos;
            //end = other.GetComponent<Jump>().endPos;

            float offset = speed / currentSpeed;
            if (offset > 0.5)
                offset = 0;

            StartCoroutine(AnimateAlongParabola(gameObject, internalPoint.transform.position, end.transform.position, 1.3f - offset, "run", "jump", true));

        }
        else if (other.tag == "Climb")
        {
            move = false;
            climb = true;

            animator.SetBool("climb", true);
            animator.SetBool("jump", false);
            animator.SetBool("run", false);

            GetComponent<Rigidbody>().isKinematic = true;
        }
        else if (other.tag == "Ladder")
        {
            move = false;
            climb = true;

            animator.SetBool("climb", false);
            animator.SetBool("jump", false);
            animator.SetBool("run", false);
            animator.SetBool("ladder", true);

            GetComponent<Rigidbody>().isKinematic = true;
        }
        else if (other.tag == "Move")
        {
            //animator.SetBool("climbEnd", true);
            animator.SetBool("run", true);
            animator.SetBool("climbEnd", false);

            animator.SetBool("climb", false);

            GetComponent<Rigidbody>().isKinematic = false;
            move = true;
            climb = false;
        }
        else if (other.tag == "Obstacle")
        {
            animator.SetBool("jump", true);
            animator.SetBool("run", false);

            move = false;
            climb = false;

            StartCoroutine(AnimateAlongParabola(gameObject, interpolObstacle.transform.position, endObstacle.transform.position, .5f, "run", "jump", true));

        }
        else if (other.tag == "WallJump")
        {
            animator.SetBool("jump", true);
            animator.SetBool("run", false);

            move = false;
            climb = false;

            StartCoroutine(AnimateAlongParabola(gameObject, interpolWall.transform.position, endWall.transform.position, .7f));

        }
        else if (other.tag == "EndClimb")
        {
            animator.SetBool("climb", false);
            animator.SetBool("ladder", false);
            animator.SetBool("climbEnd", true);

        }

        if (other.tag == "EndTrigger")
        {
            EndGame();
        }
        else if (other.tag == "EndTrigger2")
        {
            endRun = false;
            EndGameRunEnd();
        }
    }

    public void EndGame()
    {
        //move = false;
        climb = false;
        myCG.alpha = 1;
        flash = true;
        if (GridManager.Instance.gameObject.activeInHierarchy)
        {
            GridManager.Instance.gameObject.SetActive(false);
        }
        

        //animator.SetBool("jump", false);
        //animator.SetBool("run", false);

        GameManager.instance.EndType(0);

        if (GameManager.instance.isWon)
        {
            //animator.SetBool("won", true);
            endRun = true;

            //slider.value = (currentSpeed - speed) / (maxSpeed - speed);
            currentSpeed = ((maxSpeed - speed) * slider.value) + speed;

        }
        else
        {
            animator.SetBool("jump", false);
            animator.SetBool("run", false);
            animator.SetBool("lose", true);
            move = false;
            EndGameRunEnd();

        }

        isSpeeding = false;
        particle.gameObject.SetActive(true);
    }

    IEnumerator AnimateAlongParabola(GameObject go, Vector3 internalPoint, Vector3 endPoint, float time = 1.0f, string animationKey = null, string animationKey2 = null, bool moveAgain = false)
    {
        Vector3 basePos = go.transform.position;
        float x1 = 0;
        float y1 = 0;
        float z1 = 0;
        float x2 = internalPoint.x;
        float y2 = internalPoint.y;
        float z2 = internalPoint.z;
        float x3 = endPoint.x;
        float y3 = endPoint.y;
        float z3 = endPoint.z;

        float denom = (z1 - z2) * (z1 - z3) * (z2 - z3);
        float A = (z3 * (y2 - y1) + z2 * (y1 - y3) + z1 * (y3 - y2)) / denom;
        float B = (z3 * z3 * (y1 - y2) + z2 * z2 * (y3 - y1) + z1 * z1 * (y2 - y3)) / denom;
        float C = (z2 * z3 * (z2 - z3) * y1 + z3 * z1 * (z3 - z1) * y2 + z1 * z2 * (z1 - z2) * y3) / denom;

        float distance = z3 - z1;

        for (float passed = 0.0f; passed < time;)
        {
            passed += Time.deltaTime;
            float f = passed / time;
            if (f > 1) f = 1;

            float z = distance * f;
            float y = A * z * z + B * z + C;
            go.transform.position = Change.Z(go.transform.position, basePos.z + z);
            go.transform.position = Change.Y(go.transform.position, basePos.y + y);
            //Debug.Log("a");
            yield return 0;
        }

        if (moveAgain)
        {
            //landing.Play();
            Vector3 newpos = new Vector3(transform.position.x, transform.position.y - 2, transform.position.z);
            GameObject effectPlayer = (GameObject)Instantiate(landing.gameObject, landing.transform.position, transform.rotation);
            move = true;
        }

        if (animationKey != null)
            animator.SetBool(animationKey, true);
        if (animationKey2 != null)
            animator.SetBool(animationKey2, false);


    }


}
