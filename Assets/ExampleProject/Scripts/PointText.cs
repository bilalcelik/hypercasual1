using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class PointText : MonoBehaviour
{
    [SerializeField] TMP_Text pointTextPrefab;
    [SerializeField] Transform assasin;

    public static PointText instance;

    private void Awake()
    {
        instance = this;
    }

    public void PointTrigger(int countVariable)
    {
        TMP_Text assasinPoint = Instantiate(pointTextPrefab);

        assasinPoint.text = "+" + countVariable;

        assasinPoint.transform.DOScale(new Vector3(0.7f, 0.7f, 0.7f), 0.5f);

        assasinPoint.transform.position = new Vector3(assasinPoint.transform.position.x, assasin.position.y + 2, assasin.position.z + 8);

        Tween t = assasinPoint.transform.DOMoveY(9, 0.8f);

        t.OnComplete(() =>
        {
            Destroy(assasinPoint.gameObject);
        });
    }
}
