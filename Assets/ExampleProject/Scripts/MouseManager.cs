using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseManager : MonoBehaviour
{
    public static MouseManager instance;

    public bool mouseHeld = false, mouseLock = false;

    public Action UnSelectTiles;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButton(0))
        {
            if (!mouseLock)
            {
                //Debug.Log("Held");
                mouseHeld = true;
                mouseLock = true;
            }

        }
        else
        {

            if (mouseLock)
            {
                //Debug.Log("Not held");
                mouseHeld = false;
                mouseLock = false;
                UnSelectTiles.Invoke();
                GridManager.Instance.CheckSelectedBoxed();
                //GridManager.Instance.CheckSelectedImages();
            }

        }

    }

}
