using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{

    public TextMeshProUGUI levelText;
    public TextMeshProUGUI endLevelStatus;
    int levelCount;

    // Start is called before the first frame update
    void Start()
    {
        levelText.text = "Level " + PlayerPrefs.GetInt("levelCount", 1);
        levelCount = PlayerPrefs.GetInt("levelCount", 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
