using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomOpponentSelection : MonoBehaviour
{
    [SerializeField]
    string[] boardNameList = { "Leola","Collette", "Harland","Eustolia",
                        "Erminia","Collen", "DELLA","CARLEEN","Cassi","Rosina","Ulrike", "Tawny","Letha",
                        "Precious","GARY", "SHERLY","Lawanna","Patrina","Kimberely","Nicolasa", "Denisha","Gilberte","NELL",
                        "Letisha","SIBYL", "Alonso","Allena","Argelia","Michelle","Ileana", "Jarrod","Annetta","NIKOLE",
                        "Genoveva","HILDE", "Milford","Cyrus","Robena","Lenita","OWEN", "Yessenia","PIA","Brinda",
                        "Senaida","ADELA", "DERICK","Elenore","Breana","Shiloh","REY"};

    [SerializeField] List<Sprite> gamersIcon = new List<Sprite>();

    [SerializeField] GameObject you, rival, startGameButton, VS, gridPanel, gameOverPanel, leaderBoard, tapToContunie, boardObjectPrefab;

    [SerializeField] Transform leaderBoardContent;

    [SerializeField] Image mainRivalImage;

    [SerializeField] Text rivalNameText, reward;

    Vector3 youPos, rivalPos;

    Vector3 youPosS, rivalPosS;

    bool isActive = false, isControl = true;

    string botName;

    int timer = 80;

    public static RandomOpponentSelection instance;

    int values, rnd;

    float a;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        rnd = PlayerPrefs.GetInt("Order", 0) == 0 ? Random.Range(1000, 1500) : PlayerPrefs.GetInt("Order");

        //rnd = 100;

        PlayerPrefs.SetInt("Order", rnd);

        youPos = you.transform.position;

        rivalPos = rival.transform.position;

        youPosS = youPos;

        rivalPosS = rivalPos;

        gridPanel.SetActive(false);

        gameOverPanel.SetActive(false);

        PosReset();

    }

    public void StartGame()
    {
        isActive = true;
    }

    public void LeaderBoardStatus(bool isActive)
    {

        leaderBoard.SetActive(isActive);

        tapToContunie.SetActive(!isActive);

    }

    private void Update()
    {
        if (GameManager.instance.gameState != GameState.end)
        {

            a += Time.deltaTime * 10f;

            reward.text = a.ToString("00") + "\nREWARD";
        }

        if (GameManager.instance.gameState == GameState.end && isControl)
        {

            PlayerPrefs.SetInt("Money", PlayerPrefs.GetInt("Money") + (int)a);

            RandomWinList();

            isControl = false;

        }
    }

    private void FixedUpdate()
    {

        if (timer >= 0 && isActive)
        {
            timer--;

            VS.SetActive(true);

            StartCoroutine(WaitIconRandomStop());

            NewPos();

            startGameButton.gameObject.SetActive(true);

            System.Threading.Thread.Sleep(20);

            mainRivalImage.sprite = gamersIcon[Random.Range(0, gamersIcon.Count)];

            botName = boardNameList[Random.Range(0, boardNameList.Length)].ToUpper();

            Bot.instance.botNameText.text = botName.ToLower();

            rivalNameText.text = botName;

        }

    }

    IEnumerator WaitIconRandomStop()
    {

        yield return new WaitForSeconds(2f);

        isActive = false;

        yield return new WaitForSeconds(1f);

        gridPanel.SetActive(true);

        PosReset();

        VS.SetActive(false);

        startGameButton.gameObject.SetActive(false);

        Player.instance.StartGameWait();

        Bot.instance.StartGameWait();
        
    }

    private void NewPos()
    {
        you.transform.position = youPosS;

        rival.transform.position = rivalPosS;
    }

    private void PosReset()
    {
        you.transform.position = new Vector3(200000, youPos.y, youPos.z);

        rival.transform.position = new Vector3(200000, rivalPos.y, rivalPos.z);
    }

    IEnumerator Arrangement(GameObject obj)
    {
        yield return new WaitForSeconds(2f);

        for (int i = obj.transform.GetSiblingIndex(); i >= 0; i--)
        {
            yield return new WaitForSeconds(.05f);

            values--;
           
            obj.transform.GetChild(0).GetComponent<Text>().text = "#" + values + " " + "YOU";

            obj.transform.SetSiblingIndex(i);

            if (rnd > 10)
            {
                PlayerPrefs.SetInt("Order", PlayerPrefs.GetInt("Order") != 1 ? PlayerPrefs.GetInt("Order") - 1 : PlayerPrefs.GetInt("Order"));
            }
        }
    }

    private void RandomWinList()
    {
        int rnd2 = Random.Range(0, boardNameList.Length-1);

        for (int i = 0; i < 10; i++)
        {
            rnd++;

            rnd2++;

            if (9 == i)
            {
                values = rnd;

                boardObjectPrefab.transform.GetChild(0).GetComponent<Text>().text = "#" + rnd + " YOU";

                GameObject obj = Instantiate(boardObjectPrefab, leaderBoardContent);

                obj.name = rnd.ToString();

                obj.transform.GetChild(1).gameObject.SetActive(true);

                obj.transform.GetChild(2).gameObject.SetActive(true);

                StartCoroutine(Arrangement(obj));
            }
            else
            {
                rnd2 = rnd2 == 50 ? 0 : rnd2;

                string name = boardNameList[rnd2];

                boardObjectPrefab.transform.GetChild(0).GetComponent<Text>().text = "#" + rnd + " " + name;

                GameObject obj = Instantiate(boardObjectPrefab, leaderBoardContent);

                obj.name = rnd + "-" + name;
            }
        }
    }
}
