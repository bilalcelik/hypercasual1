using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    public GameObject StartPanel;

    public GameState gameState;

    public bool isWon, endLock = false;

    public Action StartGame;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        //PlayerPrefs.SetInt("Order", 20);

        gameState = GameState.start;
    }

    public void StartTheGame()
    {
        gameState = GameState.play;
        StartGame.Invoke();

        StartPanel.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void EndType(int i)
    {
        if (!endLock)
        {
            endLock = true;
            gameState = GameState.end;

            if (i == 0)
            {
                isWon = true;

            }

            else
            {
                isWon = false;
            }
        }

    }

    public void EndGame()
    {
        GridManager.Instance.GameOverMenu.transform.localScale = Vector3.zero;
        GridManager.Instance.GameOverMenu.SetActive(true);

        if (isWon)
        {
            FindObjectOfType<UIManager>().endLevelStatus.text = "Win";
            RandomOpponentSelection.instance.LeaderBoardStatus(true);
        }

        else
        {
            FindObjectOfType<UIManager>().endLevelStatus.text = "Lose";
            RandomOpponentSelection.instance.LeaderBoardStatus(false);
        }

        GridManager.Instance.GameOverMenu.transform.DOScale(Vector3.one, .5f).SetEase(Ease.OutBack);
    }
}

public enum GameState
{
    start,
    play,
    end
}
