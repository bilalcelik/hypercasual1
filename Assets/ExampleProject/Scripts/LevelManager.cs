using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public bool isFirstLevelManager = false;
    // Start is called before the first frame update
    void Start()
    {
        if (isFirstLevelManager)
        {
            SceneManager.LoadScene(PlayerPrefs.GetInt("levelIndex", 2));
        }
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void NextLevel()
    {
        if (GameManager.instance.isWon)
        {
            if (SceneManager.GetActiveScene().buildIndex + 1 < SceneManager.sceneCountInBuildSettings)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                PlayerPrefs.SetInt("levelCount", PlayerPrefs.GetInt("levelCount", 1) + 1);
                PlayerPrefs.SetInt("levelIndex", PlayerPrefs.GetInt("levelIndex", 1) + 1);
            }
            else
            {
                SceneManager.LoadScene(2);
                PlayerPrefs.SetInt("levelIndex", 1);
            }
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

    }
}
