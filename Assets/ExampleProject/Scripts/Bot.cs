using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Bot : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject internalPoint, end, interpolObstacle, endObstacle, interpolWall, endWall;

    public bool move = true, climb = false;
    public float speed = 1;
    Animator animator;

    [SerializeField] public TMP_Text botNameText;

    [SerializeField]
    string[] botName = { "Mike Gold", "Don Box",
                        "Sundar Lal", "Neel Beniwal" };

    public static Bot instance;

    private void Awake()
    {
        instance = this;
    }

    void Start()
    {


        animator = GetComponent<Animator>();

        move = false;


        GameManager.instance.StartGame += StartTheGame;
        animator.SetBool("idle", true);
        animator.SetBool("run", false);
    }

    void StartTheGame()
    {
        //Etkisiz Eleman Ama silme sonradan başın ağırır ne işe yaradıgını bilmiyorsun (küçük bir uyarı!!!)
    }

    public void StartGameWait()
    {
        move = true;

        animator.SetBool("idle", false);

        animator.SetBool("run", true);
    }

    // Update is called once per frame
    void Update()
    {
        // Move the object forward along its z axis 1 unit/second.
        if (move)
            transform.Translate(Vector3.forward * Time.deltaTime * speed);

        if (climb)
        {
            transform.Translate(Vector3.up * Time.deltaTime * speed);

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Jump")
        {
            animator.SetBool("jump", true);
            animator.SetBool("run", false);

            move = false;
            climb = false;


            StartCoroutine(AnimateAlongParabola(gameObject, internalPoint.transform.position, end.transform.position, 1.3f, "run", "jump", true));

        }
        else if (other.tag == "Climb")
        {
            move = false;
            climb = true;

            animator.SetBool("climb", true);
            animator.SetBool("jump", false);
            animator.SetBool("run", false);

            GetComponent<Rigidbody>().isKinematic = true;
        }
        else if (other.tag == "Ladder")
        {
            move = false;
            climb = true;

            animator.SetBool("climb", false);
            animator.SetBool("jump", false);
            animator.SetBool("run", false);
            animator.SetBool("ladder", true);

            GetComponent<Rigidbody>().isKinematic = true;
        }
        else if (other.tag == "Move")
        {
            //animator.SetBool("climbEnd", true);
            animator.SetBool("run", true);
            animator.SetBool("climbEnd", false);

            animator.SetBool("climb", false);

            GetComponent<Rigidbody>().isKinematic = false;
            move = true;
            climb = false;
        }
        else if (other.tag == "Obstacle")
        {
            animator.SetBool("jump", true);
            animator.SetBool("run", false);

            move = false;
            climb = false;

            StartCoroutine(AnimateAlongParabola(gameObject, interpolObstacle.transform.position, endObstacle.transform.position, .5f, "run", "jump", true));

        }
        else if (other.tag == "WallJump")
        {
            animator.SetBool("jump", true);
            animator.SetBool("run", false);


            move = false;
            climb = false;

            StartCoroutine(AnimateAlongParabola(gameObject, interpolWall.transform.position, endWall.transform.position, .7f));

        }
        else if (other.tag == "EndClimb")
        {
            animator.SetBool("climb", false);
            animator.SetBool("ladder", false);
            animator.SetBool("climbEnd", true);

        }

        if (other.tag == "EndTrigger")
        {
            move = false;
            climb = false;

            animator.SetBool("jump", false);
            animator.SetBool("run", false);

            GameManager.instance.EndType(1);

            if (!GameManager.instance.isWon)
                animator.SetBool("won", true);
            else
                animator.SetBool("lose", true);

        }
    }

    IEnumerator AnimateAlongParabola(GameObject go, Vector3 internalPoint, Vector3 endPoint, float time = 1.0f, string animationKey = null, string animationKey2 = null, bool moveAgain = false)
    {
        Vector3 basePos = go.transform.position;
        float x1 = 0;
        float y1 = 0;
        float z1 = 0;
        float x2 = internalPoint.x;
        float y2 = internalPoint.y;
        float z2 = internalPoint.z;
        float x3 = endPoint.x;
        float y3 = endPoint.y;
        float z3 = endPoint.z;

        float denom = (z1 - z2) * (z1 - z3) * (z2 - z3);
        float A = (z3 * (y2 - y1) + z2 * (y1 - y3) + z1 * (y3 - y2)) / denom;
        float B = (z3 * z3 * (y1 - y2) + z2 * z2 * (y3 - y1) + z1 * z1 * (y2 - y3)) / denom;
        float C = (z2 * z3 * (z2 - z3) * y1 + z3 * z1 * (z3 - z1) * y2 + z1 * z2 * (z1 - z2) * y3) / denom;

        float distance = z3 - z1;

        for (float passed = 0.0f; passed < time;)
        {
            passed += Time.deltaTime;
            float f = passed / time;
            if (f > 1) f = 1;

            float z = distance * f;
            float y = A * z * z + B * z + C;
            go.transform.position = Change.Z(go.transform.position, basePos.z + z);
            go.transform.position = Change.Y(go.transform.position, basePos.y + y);
            //Debug.Log("a");
            yield return 0;
        }


        if (moveAgain)
            move = true;


        if (animationKey != null)
            animator.SetBool(animationKey, true);
        if (animationKey2 != null)
            animator.SetBool(animationKey2, false);
    }
}
