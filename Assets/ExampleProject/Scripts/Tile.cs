﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    private static Tile selected;
    public SpriteRenderer Renderer;

    public Vector2Int Position;

    public bool isSelected = false;

    public bool isEmpty = false, isObstacle = false;

    public GameObject top, rightTop, right, rightBot, bot, leftBot, left, leftTop;

    public GameObject[] bars = new GameObject[8];

    GameObject selectedBar;

    private void Start()
    {
        Renderer = GetComponent<SpriteRenderer>();

        MouseManager.instance.UnSelectTiles += Unselect;

        for (int i = 0; i < 8; i++)
        {
            bars[i] = transform.parent.GetChild(1).GetChild(i).gameObject;
        }

        
    }

    public void Select(Vector2Int lastPosition)
    {
        //Debug.Log(lastPosition);

        if (lastPosition.x != -1)
        {
            if (lastPosition.x > Position.x)
            {
                if (lastPosition.y > Position.y)
                {
                    // right top
                    //rightTop.SetActive(true);
                    bars[1].SetActive(true);
                    selectedBar = rightTop;
                }
                else if (lastPosition.y < Position.y)
                {
                    // right bot
                    //rightBot.SetActive(true);
                    bars[3].SetActive(true);
                    selectedBar = rightBot;
                }
                else
                {
                    //right.SetActive(true);
                    bars[2].SetActive(true);
                    selectedBar = right;
                }
            }
            else if (lastPosition.x < Position.x)
            {
                if (lastPosition.y > Position.y)
                {
                    // left top
                    //leftTop.SetActive(true);
                    bars[7].SetActive(true);
                    selectedBar = leftTop;
                }
                else if (lastPosition.y < Position.y)
                {
                    // left bot
                    //leftBot.SetActive(true);
                    bars[5].SetActive(true);
                    selectedBar = leftBot;
                }
                else
                {
                    //left.SetActive(true);
                    bars[6].SetActive(true);
                    selectedBar = left;
                }
            }
            else
            {
                if (lastPosition.y > Position.y)
                {
                    // top
                    //top.SetActive(true);
                    bars[0].SetActive(true);
                    selectedBar = top;
                }
                else if (lastPosition.y < Position.y)
                {
                    // bot
                    //bot.SetActive(true);
                    bars[4].SetActive(true);
                    selectedBar = bot;
                }

            }
        }

        Renderer.color = Color.grey;
        isSelected = true;
    }

    public void Unselect()
    {
        Renderer.color = Color.white;
        isSelected = false;

        for (int i = 0; i < 8; i++)
        {
            bars[i].SetActive(false);

        }
    }


    public void OnMouseOver()
    {
        // FAREYE BASILMIŞ VE BU TİLE SEÇİLİ DEĞİLSE
        if (MouseManager.instance.mouseHeld && !isSelected && !isObstacle)
        {
            // DAHA ÖNCE SEÇİLEN Bİ TİLE VARSA
            if (GridManager.Instance.SelectedBoxes.Count > 0)
            {
                // İLK SEÇİLMİŞ TİLE İLE AYNI SPRİTE İSE
                if (Renderer.sprite == GridManager.Instance.SelectedBoxes[0].sprite)
                {
                    int lastTileIndex = GridManager.Instance.SelectedBoxes.Count - 1;
                    if (lastTileIndex < 0)
                        lastTileIndex = 0;

                    Tile LastTile = GridManager.Instance.SelectedBoxes[lastTileIndex].GetComponent<Tile>();
                    // SON SEÇİLMİŞ TİLE'A KOMŞU İSE
                    if (Vector2Int.Distance(LastTile.Position, Position) < 2)
                    {
                        Select(LastTile.Position);
                        GridManager.Instance.SelectedBoxes.Add(Renderer);
                    }

                }
            }
            else
            {
                Select(Vector2Int.one * -1);
                GridManager.Instance.SelectedBoxes.Add(Renderer);
            }


        }
    }

    private void OnMouseDown()
    {
        // FAREYE BASILMIŞ VE BU TİLE SEÇİLİ DEĞİLSE
        if (MouseManager.instance.mouseHeld && !isSelected && !isObstacle)
        {
            // DAHA ÖNCE SEÇİLEN Bİ TİLE VARSA
            if (GridManager.Instance.SelectedBoxes.Count > 0)
            {
                // İLK SEÇİLMİŞ TİLE İLE AYNI SPRİTE İSE
                if (Renderer.sprite == GridManager.Instance.SelectedBoxes[0].sprite)
                {
                    int lastTileIndex = GridManager.Instance.SelectedBoxes.Count - 1;
                    if (lastTileIndex < 0)
                        lastTileIndex = 0;

                    Tile LastTile = GridManager.Instance.SelectedBoxes[lastTileIndex].GetComponent<Tile>();

                    // SON SEÇİLMİŞ TİLE'A KOMŞU İSE
                    if (Vector2Int.Distance(LastTile.Position, Position) < 2)
                    {
                        Select(LastTile.Position);
                        GridManager.Instance.SelectedBoxes.Add(Renderer);
                    }

                }
            }
            else
            {
                Select(Vector2Int.one * -1);
                GridManager.Instance.SelectedBoxes.Add(Renderer);
            }


        }

       
    }
}
